#! /usr/bin/env python
'''
viewer for mat files.

Example:

$ ~/python/shellscripts/matfileviewer.py -i some_file.mat --header



author: Markus
'''


import argparse
import h5py
import logging
from numpy_utils import loadmat
import os
import sys

import numpy as np
logging.basicConfig(level=logging.INFO,
                    format='%(levelname)-8s %(message)s',
                    )


def main():

    try:
        # parsing
        args = parse()

        # read file and show content.
        read_and_show_content(args)
    except Exception as e:
        print 'ERROR\t', e.msg
        return 1
    return 0


def parse():
    '''parse command line arguments (and do some sanity checks?).
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='input file [.mat]')
    parser.add_argument('--header', help='show header',
                        action='store_true')
    parser.add_argument('--full', help='show full data',
                        action='store_true')

    args = parser.parse_args()

    return args


def read_and_show_content(args):
    '''tries to load the mat file and show its content.
    '''

    if not os.path.isfile(args.input):
        logging.error('Given file does not exist!')
        raise Exception()

    if not os.path.splitext(args.input)[1] == '.mat':
        logging.error('Given file is not a .mat file!')
        raise Exception()

    # TODO: move hdf5 loader into another package such that is reusable.
    try:
        fin = loadmat(args.input, squeeze_me=True)
        print ''
        print 'File: {}'.format(args.input)
        print ''

        if args.header:
            print_default_keys(fin)

        print_content_information(fin)

        if args.full:
            print_full(fin)

    except Exception as e:
        raise e


def print_default_keys(fin):
    '''prints header information.
    '''

    print 'Header information:'
    if fin.get('__header__') is None:
        print '\tnot present! (<v7.3 matfile)'
        return

    print '\t' + '\n\t'.join(fin.get('__header__').split(', '))
    print 'Version: {}'.format(fin.get('__version__'))
    print ''


def print_content_information(fin):
    '''prints information about all the actual data stored.
    '''

#     if len(fin.keys()) == 3:
#         print 'given file does not contain any data!'
#         return

    print 'Data:'
    fmt = '\t{:<20} : {:20}'
    print fmt.format('IDENTIFIER', 'SPECS')

    for key, value in fin.iteritems():

        # skip default keys.
        if key in ['__version__', '__header__', '__globals__']:
            continue

        if isinstance(value, np.ndarray):
            #             print key, value.shape
            print fmt.format(key,
                             describe_ndarray(value))
        # untested.
        elif isinstance(value, str):
            print fmt.format(key, value)
        elif type(value) in (int, float, bool):
            print fmt.format(key, str(value))
        else:
            print fmt.format(key, type(value))


def print_full(fin):
    if len(fin.keys()) == 3:
        print 'given file does not contain any data!'
        return

    print ''
    print 'Data (full):'

    for key, value in fin.iteritems():
        # skip default keys.
        if key in ['__version__', '__header__', '__globals__']:
            continue

        print '\t{}'.format(key)
        print value


def describe_ndarray(value):
    '''generates a string that describes an ndarray.
    '''
    desc = 'ndarray ' + '(' + \
        'x'.join([str(x) for x in value.shape]) \
        + ', ' + str(value.dtype)
    try:
        desc += ', [' + '{:2.1f}'.format(value.min()) \
            + ', ' + '{:2.1f}'.format(value.max()) + ']' \
            + ')'
    except Exception as e:
        desc += ')'
    finally:
        return desc


if __name__ == '__main__':
    sys.exit(main())
