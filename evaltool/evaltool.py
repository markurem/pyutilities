'''
Tools to evaluate segmentations.

@author: markus
'''
from warnings import warn
warn('This module is no longer supported!', DeprecationWarning)

import myplt
from sklearn.metrics import jaccard_similarity_score, f1_score, \
    precision_score, recall_score

import matplotlib.pyplot as plt
import numpy as np


__created__ = '22.12.2014'
__updated__ = '26.08.2015'


SCORING = {'jaccard': jaccard_similarity_score,
           'dice': f1_score,
           'precision': precision_score,
           'recall': recall_score}

colors = ['lightskyblue',
          'indianred',
          'seagreen',
          'steelblue',
          'orange',
          ]


def evaluate(prediction, ground_truth, scoring=None):
    '''compute several scores for the given prediction and ground truth.

    Parameters
    ----------
    prediction : array_like
        predictions to be evaluated.
    ground_truth : array_like
        ground truth values to be compared against.

    Returns
    -------
    scores : dict
        achieved scores.
    '''

    if not prediction.shape == ground_truth.shape:
        raise ValueError('Dimensions of prediction and ground truth mismatch!'
                         + ' ' + str(prediction.shape) + ' != ' +
                         str(ground_truth.shape))

    prediction = prediction.ravel()
    ground_truth = ground_truth.ravel()

    if scoring is None:
        scoring = SCORING
    elif scoring in SCORING.keys():
        scoring = {scoring: SCORING[scoring]}

    scores = {}
    for score_name, scorer in scoring.iteritems():
        scores[score_name] = scorer(ground_truth, prediction)

    return scores


def evaluate_multilabel(prediction, ground_truth, scoring=None):
    '''compute several scores for the given prediction and ground truth.
    This function computes scores for each label separately (1 vs all).

    Parameters
    ----------
    prediction : array_like
        predictions to be evaluated.
    ground_truth : array_like
        ground truth values to be compared against.

    Returns
    -------
    scores : recarray
        achieved scores, indices correspond to labels. Scores
        can be accessed by their field names.

    Examples
    --------
    >>> scores = evaluate_multilabel(pred, ground_truth)
    >>> for name in scores.dtype.names:
    >>>     print name, scores[name]

    >>> for label in xrange(ground_truth.max() + 1):
    >>>     print scores[label]
    '''

    unique = np.unique(ground_truth)
    n_labels = len(unique)

    if scoring is None:
        scoring = SCORING
    elif scoring in SCORING.keys():
        scoring = {scoring: SCORING[scoring]}

    scores = np.recarray(
        shape=[n_labels, ],
        dtype=[(score_name, float) for score_name in scoring.keys()])

    for label in unique:
        labelscores = evaluate(prediction == label,
                               ground_truth == label,
                               scoring=scoring)
        for score_name, score_val in labelscores.iteritems():
            scores[label][score_name] = score_val

    return scores


def correct_matlab_labeling(array):
    '''shifts all indices such that the first label is 0 instead of 1.

    Parameters
    ----------
    array : array_like
        label array.

    Returns
    -------
    array : array_like
        label array with python numeration.
    '''

    if array.min() == 1:
        return array - 1
    elif array.min() > 1:
        warn('first label is larger than 1!')
        return array - 1
    else:
        raise ValueError('negative labels encountered!')


def show_multilabel_scores(scores, skipzero=True, score_name='dice',
                           xticklabels=None):
    '''visualizes achieved scores for each label.

    Works well with the output of evaluate_multilabel!

    Parameters
    ----------
    scores : recarray
        scores to be visualized.
    skipzero : bool, optional (default: True)
        do not show scores for label 0 (background).
    score_name : string
        determines which score to be plotted.
    '''

    # we transpose here as this function was written for a different case
    # than show_trend_scores but it's better to have a common interface.
    scores = scores.T

    n_labels = scores.shape[0]
    labels = np.arange(n_labels)
    span = 0.9

    if skipzero:
        labels = labels[1:]
        width = span / (n_labels - 1)
    else:
        width = span / n_labels

    halfspan = span / 2.0
    if len(scores.shape) > 1:
        ind = np.arange(scores.shape[1]) - halfspan + 1
    else:
        ind = [1 - halfspan, ]

    # make a bar for each label at every index. Each label has its own color.
    for ii, label in enumerate(labels):
        plt.bar(ind + ii * width,
                scores[label][score_name],
                width=width,
                color=colors[ii], linewidth=0, alpha=0.8)

    # cosmetics.
    myplt.remove_border()
    plt.grid(axis='y')
    ax = plt.gca()
#     ax.set_yticks([0, 0.25, 0.5, 0.75, 1])
#     ax.set_yticklabels([0, '', 0.5, '', 1])
    ax.set_ylabel(score_name)
    ax.set_xticks(ind + halfspan)
    if xticklabels is not None:
        ax.set_xticklabels(xticklabels)


def show_trend_scores(scores, skipzero=True, score_name='dice',
                      indices=None):
    '''visualizes score along the second dimension of scores.

    Parameters
    ----------
    scores : recarray, shape=[n_items, n_labels]
        scores to be visualized.
    skipzero : bool, optional (default: True)
        do not show scores for label 0 (background).
    score_name : string
        determines which score to be plotted.
    '''

    if not len(scores.shape) == 2:
        raise ValueError('scores array is not 2d!')

    n_labels = scores.shape[1]
    labels = np.arange(n_labels)

    if indices is None:
        indices = np.arange(scores.shape[0])

    if skipzero:
        labels = labels[1:]

    for ii, label in enumerate(labels):
        plt.plot(indices, scores[:, label][score_name],
                 color=colors[ii])

    myplt.remove_border()
    plt.grid(axis='y')
    ax = plt.gca()
    ax.set_yticks([0, 0.25, 0.5, 0.75, 1])
    ax.set_yticklabels([0, '', 0.5, '', 1])
    ax.set_ylabel(score_name)

if __name__ == '__main__':
    print '=== TESTING EVALTOOL ==='
    ground_truth = np.zeros((10, 10), dtype=int)
    ground_truth[2:4, 2:4] = 1
    ground_truth[4:6, 6:] = 2
    ground_truth[6:, :5] = 3

    scores = evaluate_multilabel(ground_truth, ground_truth)
    print 'identical:', scores

    np.random.seed(1337)
    pred = ground_truth + (np.random.randn(10, 10) > 1)
    pred[pred > ground_truth.max()] = 2

    scores = evaluate_multilabel(pred, ground_truth)
    print 'noisy:'
    for name in scores.dtype.names:
        print name, scores[name]

    for label in xrange(ground_truth.max() + 1):
        print scores[label]

    score_stack = np.vstack((scores, ) * 4)
    print score_stack.shape
    show_multilabel_scores(score_stack)

    plt.figure()
    show_trend_scores(
        score_stack, score_name='dice')

    plt.show()
