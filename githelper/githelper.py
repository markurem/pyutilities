import os
import git

from subprocess import check_output, CalledProcessError
import os.path



def get_branch_and_rev(path=None):
    '''gets both branch name and revision of the 
    given repository.
    '''
    if path is None:
        try:
            repo = git.Repo(os.getcwd())
        except:
            repo = git.Repo(root())

    return dict(dir=repo.git_dir,
                branch=repo.active_branch.name,
                rev=repo.active_branch.object.name_rev)


def get_branch_and_rev_as_str(path=None):
    '''returns a string of branch and revision.
    '''
    return 'branch={branch}, rev={rev}'.format(
        **get_branch_and_rev(path=path))


def root():
    ''' returns the absolute path of the repository root '''
    try:
        base = check_output(['git', 'rev-parse', '--show-toplevel'])
    except CalledProcessError:
        raise IOError('Current working directory is not a git repository')
    return base.decode('utf-8').strip()