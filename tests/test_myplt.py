'''
unit test for myplt module.
'''

import myplt
from numpy.testing import assert_array_equal
import unittest

import matplotlib.pyplot as plt
import numpy as np
__created__ = '13.03.2015'
__updated__ = '23.06.2015'


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        plt.close('all')

    def test_boxplot(self):
        '''test boxplot
        '''

        x = np.random.randn(10, 200)
        for ii in xrange(10):
            x[ii] += ii
        myplt.boxplot(x.T, color='lightskyblue')
        myplt.savefig('/tmp/test_myplt/boxplot.png')

    def test_percentile_plot(self):
        '''test percetile plot
        '''

        n = 25
        x = np.random.randn(n, 200) + np.sin(
            np.linspace(0, 2 * np.pi, n))[..., np.newaxis]
        plt.autumn()
        for delta in [0, 1, 5, 10, 25]:
            myplt.percentile_plot(x, percentile=[delta, 100 - delta])
        myplt.savefig('/tmp/test_myplt/percentile_plot.png')

    def test_multiclass_(self):
        '''test multiclass roc plot
        '''

        labels = np.random.choice(3, size=100)
        scores = np.random.randn(100, 3)
        scores -= scores.min()
        scores = scores / scores.sum(axis=-1)[..., None]
        myplt.multiclass_roc(labels, scores, average=True)
        plt.legend()
        myplt.savefig('/tmp/test_myplt/roc_plot.png')
        plt.figure()
        myplt.multiclass_prc(labels, scores, average=True)
        plt.legend()
        myplt.savefig('/tmp/test_myplt/pr_plot.png')

    def test_imwrite(self):
        '''test imwrite
        '''

        image = np.random.randn(100, 100) * 100
        image -= image.min()
        image /= image.max() * 255
        image = image.astype(np.uint8)
        mask = np.zeros_like(image)
        mask[25:55, 10:80] = 1
        mask[10:20, 10:20] = 2

        myplt.imwrite('/tmp/test_myplt/imwrite.png', image)
        myplt.imwrite('/tmp/test_myplt/imwrite_with_mask.png', image, mask)

    def test_correlation_plot(self):
        '''test correlation plot
        '''

        n = 100
        x = np.random.randn(n,) * 10 + 5
        y = x * 2 - 0.075 * x ** 2 + 13 + np.random.randn(n,) * 15

        labels = [str(ii) for ii in np.random.choice(n, replace=False, size=n)]

        myplt.correlation_plot(
            x.tolist(),
            y.tolist(),
            color='indianred',
            labels=labels)
        myplt.savefig('/tmp/test_myplt/correlation_plot.png')

    def test_matshow(self):
        ''' test matshow.
        '''
        image = np.random.randn(5, 5)
        myplt.matshow(image, show_values=True,
                      cmap='RdBu_r', vmin=-3, vmax=3, fontsize=14)
        myplt.savefig('/tmp/test_myplt/matshow.png')

    def test_entropy_plot(self):

        image = np.random.randn(20, 20)
        image -= image.min()

        myplt.entropy_plot(image)
        myplt.savefig('/tmp/test_myplt/entropy_plot.png')

    def test_feature_importances(self):
        '''test feature importance plot
        '''
        importances = np.random.choice(np.linspace(0, 1, 100), size=100)
        names = [str(x) for x in xrange(len(importances))]

        _, axarr = plt.subplots(1, 3)
        for ax, params in zip(axarr, [{'sorted': False},
                                      {'sorted': True},
                                      {'sorted': True, 'top': 10}]):
            plt.sca(ax)
            myplt.feature_importance_plot(importances, names,
                                          **params)
        myplt.savefig('/tmp/test_myplt/feature_importance.png')

    def test_segmentation_difference(self):
        '''test difference map.
        '''
        shape = (12, 12)
        aa = np.zeros(shape)
        bb = np.zeros(shape)
        aa[:5, :] = 1
        bb[:, :5] = 1
        diff_aa_bb = myplt.segmentation_difference(aa, bb)
        expected = np.zeros_like(diff_aa_bb)
        expected[:5, :5, :] = (255, 255, 255)
        expected[5:, :5, :] = (255, 0, 255)
        expected[:5, 5:, :] = (255, 255, 0)
        assert_array_equal(diff_aa_bb, expected)


if __name__ == "__main__":
    unittest.main()
