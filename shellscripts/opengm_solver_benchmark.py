#! /usr/bin/env python

'''
loads an existing opengm-graphical model and tries to solve it with a set
of different optimizers.
'''

import argparse
import logging
from numpy import Infinity
import os
import platform
import sys
import time

import numpy as np
try:
    import opengm
except ImportError:
    print 'Could not import opengm. Is it installed?'
    sys.exit(1)

logging.basicConfig(level=logging.INFO,
                    datefmt='%H:%M:%S',
                    format='[%(asctime)-8s] %(message)s',
                    )


SOLVER_SETS = {'mm': {'icm': opengm.inference.Icm,
                      'alpha-beta-swap': opengm.inference.AlphaBetaSwap,
                      #                       'lazy flipper': opengm.inference.LazyFlipper,
                      'alpha-expansion-fusion': opengm.inference.AlphaExpansionFusion,
                      'fastpd': opengm.inference.FastPd,
                      },
               'mp': {'trbp': opengm.inference.TreeReweightedBp,
                      'belief propagation': opengm.inference.BeliefPropagation,
                      },
               'gc': {'alpha-exp': opengm.inference.AlphaExpansion,
                      'qpbo': opengm.inference.QpboExternal,
                      'graph-cut': opengm.inference.GraphCut,
                      },
               'search': {'brute force': opengm.inference.Bruteforce,
                          },
               'lp': {'ad3-lp': opengm.inference.Ad3,
                      'mqpbo': opengm.inference.Mqpbo,
                      'trws-ext': opengm.inference.TrwsExternal,
                      'dual-decomposition-subgrad':
                      opengm.inference.DualDecompositionSubgradient,
                      'cplex': opengm.inference.LpCplex,
                      },
               'ilp': {'ad3-ilp': opengm.inference.Ad3,
                       # 'cplex-ilp': opengm.inference.LpCplex},
                       }
               }

ABS_GAP = 1e-6
REL_GAP = 1e-8

__desc__ = '''
loads an existing opengm-graphical model and tries to solve it with a set
of different optimizers. It will report achieved objective values and
computed bounds.
'''

__doc__ = '''
Different classes of solvers can be tried by specifying the class with the
argument --solvers. Options are:

    mm : move making algorithms {mm}
            
    gc : graph cuts {gc}
    search : search algorithms {search}
    mp : message passing algorithms {mp}
    lp : linear programming relaxations and decompositions {lp}
    ilp : integer programming approaches {ilp}
    
If none is chosen, a set of default optimizers will be tested. To run all,
use the keyword 'all'.

Note that certain solvers will need problem specific parameter tuning!

An optimality certificate (*) is given when the objective value of the solution
is within an absolute gap of {abs} and a relative gap of {rel}

Example
-------
python ~/python/shellscripts/opengm_solver_benchmark.py -i /tmp/gm.hdf5 -m first --solvers lp, ilp


'''.format(abs=ABS_GAP, rel=REL_GAP,
           **{key: '\n\t  * ' + '\n\t  * '.join(name for name in value.keys())
              for key, value in SOLVER_SETS.items()})


def main():

    try:
        args = parse()
        gm = load(args)
        solvers = get_solvers(args)
        objectives = run_benchmark(gm, solvers)
        if args.output is not None:
            write_scores_to_table(objectives, args)
            np.savez(os.path.splitext(args.output)[0] + '.npz',
                     objectives=objectives)
    except Exception as e:
        logging.error('An error occured!\n%s' % str(e))
        return 1
    return 0


def parse():
    '''parse command line arguments (and do some sanity checks?).
    '''

    parser = argparse.ArgumentParser(
        description=__desc__,
        epilog=__doc__,
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('input', help='input file [.hdf5]')
    parser.add_argument('-m', '--model', help='model name.')
    parser.add_argument('-s', '--solvers', nargs='+',
                        help='solver classes [mm, ilp, lp, search, gc, mp]')
    parser.add_argument('--output', help='output file for table [.tex]',
                        default=None)
    args = parser.parse_args()

    if args.input is None:
        logging.error('No input file given!')
        raise Exception()
    return args


def load(args):

    gm = opengm.gm(0)
    opengm.hdf5.loadGraphicalModel(gm, args.input, args.model)

    print 'IMPORTED MODEL:'
    print gm

    return gm


def get_warm_start(gm):
    print ''
    logging.info('computing warmstart...')
    Inf = opengm.inference.LazyFlipper
    params = opengm.InfParam(maxSubgraphSize=1)
    inf = Inf(gm=gm, parameter=params)
    inf.infer()
    logging.info('complete! value={:1.5e}'.format(inf.value()))
    return inf.arg()


def get_solvers(args):

    if args.solvers is None:
        return {  # default solvers
            'fastpd': opengm.inference.FastPd,
            'icm': opengm.inference.Icm,
            'ad3-lp': opengm.inference.Ad3,
            'alpha-beta-swap': opengm.inference.AlphaBetaSwap,
            'lazy flipper': opengm.inference.LazyFlipper,
            'trbp': opengm.inference.TreeReweightedBp,
            'trws-ext': opengm.inference.TrwsExternal,
            'mqpbo': opengm.inference.Mqpbo,
            'dual-decomposition-subgrad':
            opengm.inference.DualDecompositionSubgradient,
            'alpha-expansion-fusion': opengm.inference.AlphaExpansionFusion,
            'belief propagation': opengm.inference.BeliefPropagation,
        }
    elif 'all' in args.solvers:
        args.solvers = ['ilp', 'search', 'gc', 'lp', 'mm', 'mp']

    solvers = {}
    if 'ilp' in args.solvers:
        solvers.update(SOLVER_SETS['ilp'])

    if 'search' in args.solvers:
        solvers.update(SOLVER_SETS['search'])

    if 'gc' in args.solvers:
        solvers.update(SOLVER_SETS['gc'])

    if 'lp' in args.solvers:
        solvers.update(SOLVER_SETS['lp'])

    if 'mm' in args.solvers:
        solvers.update(SOLVER_SETS['mm'])

    if 'mp' in args.solvers:
        solvers.update(SOLVER_SETS['mp'])

    if not solvers:
        all_solvers = {}
        for solver_set in SOLVER_SETS.values():
            all_solvers.update(solver_set)
        for solver in args.solvers:
            if solver in all_solvers.keys():
                solvers[solver] = all_solvers[solver]

    print 'found solvers:', solvers

    return solvers


def write_scores_to_table(objectives, args):

    n = len(objectives.values()[0])

    fmt = '{method} & {value:>3.6e} & {bound:>3.6e} & {runtime:>5}' + \
        '\\\\' + '\n'
    header = 'Method & Value & Bound & Runtime [s]' + '\\\\' + '\n'
# {name:<28} : {value:>15.5e} : {bound:>14.5e} : {runtime:>12}
    with open(args.output, 'w') as fout:
        # add some comment about machine.
        fout.write('%% ' + ', '.join(platform.uname()) + '\n')
        # now write the actual tex code.
        fout.write('\\begin{tabular}{' + (1 + n) * 'l' + '}' + '\n')
        fout.write(header)
        fout.write('\\hline\n')
        for name, objs in sorted(objectives.items(),
                                 key=lambda x: x[1]['value']):
            fout.write(fmt.format(method=name, **objs))

        fout.write('\\end{tabular}')


def run_benchmark(gm, solvers):

    warm_start = get_warm_start(gm)

    objectives = {  # 'ground truth': gm.evaluate(ground_truth.flatten()),
        'warm start': {'value': gm.evaluate(warm_start.flatten()),
                       'bound': -Infinity,
                       'runtime': '-'}
    }

    for name, inf in solvers.iteritems():

        # some solvers need custom parameters to run properly.
        params = {}
        if name == 'belief propagation':
            params = {'convergenceBound': 1e-6,
                      'steps': 200}
        elif name == 'ad3-ilp':
            params = {'solverType': 'ad3_ilp'}
        elif name == 'dual-decomposition-subgrad':
            params = {'maximalNumberOfIterations': 1000}
        elif name == 'cplex-ilp':
            params = {'integerConstraint': True}

        try:
            logging.info('=== %s ===' % name)
            inf = inf(gm, parameter=opengm.InfParam(**params))
    #         inf.setStartingPoint(warm_start)
            logging.info(
                'setting up optimizer complete. starting inference...')
            start = time.time()
            inf.infer(
                inf.verboseVisitor(
                    printNth=25000
                    if name in ['lazy flipper', 'icm'] else 1,
                    multiline=False))
            stop = time.time()
            logging.info('inference terminated with objective value {} and'
                         ' bound {}'.format(inf.value(), inf.bound()))
        except Exception as e:
            if 'supports only' in str(e):
                print 'solver %s does not support the given problem!' % name
                objectives[name] = {'value': Infinity,
                                    'bound': -Infinity,
                                    'runtime': '-'}
            else:
                raise
        else:
            objectives[name] = {'value': inf.value(),
                                'bound': inf.bound(),
                                'runtime': '%3.1f' % (stop - start)}

    print '{:<28} : {:>15} : {:>14} : {:>12}'.format(
        'METHOD', 'OBJECTIVE VALUE', 'BOUND', 'RUNTIME [s]')
    for name, stats in sorted(objectives.items(),
                              key=lambda x: x[1]['value']):
        if stats['value'] - stats['bound'] < ABS_GAP and \
                (stats['value'] - stats['bound']) / stats['bound'] < REL_GAP:
            name = name + '*'
        print '{name:<28} : {value:>15.5e} : {bound:>14.5e} : {runtime:>12}'.format(
            name=name, **stats)

    return objectives

if __name__ == '__main__':
    sys.exit(main())
