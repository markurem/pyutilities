'''
unit test for medvis.py

Created on Dec 1, 2014

@author: markus
'''

import medvis
import os
import unittest

import SimpleITK as itk
import numpy as np
__created__ = '01.12.2014'
__updated__ = '19.03.2015'


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_read_dicom(self):
        '''test dicom reader
        '''

        # and now do it again with a proper series.
        # TODO: prepare deployable test.
        path = '/home/markus/container/data/mra_7T_heidelberg/raw/pat1/tof_7T/TSE_T2_200um - 8'
        if not os.path.exists(path):
            print 'Testfile (dicom) not found!'
            return
        image = medvis.read_dicom(path, verbose=True)
        self.assertIsInstance(image, itk.Image)
        self.assertTrue(len(image.GetSize()) > 0)
        self.assertTrue(np.all([x > 0 for x in image.GetSize()]))

if __name__ == "__main__":
    unittest.main()
