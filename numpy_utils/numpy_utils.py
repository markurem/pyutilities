'''
Created on Jan 11, 2015

@author: Markus Rempfler
'''

import h5py
from scipy.io import loadmat as scipy_loadmat
from scipy.ndimage.interpolation import zoom
import sys
import time
import logging


import numpy as np


def create_logger(fout, name=None, fmt=None):
    '''creates a simple logger that writes its output to the given
    output file.

    Parameters
    ----------
    fout : string
        path to output file.
    name : string, optional
        logger name.
    fmt : string, optional
        formatter. Default: '%(asctime)s %(message)s'

    Returns
    -------
    logger : Logger
        logger.
    '''
    logger = logging.getLogger('myapp' if name is None else name)
    # prevent multiple outputs.
    if logger.handlers:
        logger.handlers = []
    hdlr = logging.FileHandler(fout)
    if fmt is None:
        fmt = '%(asctime)s %(message)s'
    fmt = logging.Formatter(fmt)
    hdlr.setFormatter(fmt)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger


def loadmat(fin, **kwargs):
    '''tries to load data from a mat file while handling both old and new
    formats.

    Parameters
    ----------
    fin : string
        path to matfile.

    Returns
    -------
    fin : dict
        structure containing data of matfile.
    '''

    try:
        fin = scipy_loadmat(fin, **kwargs)
    except (NotImplementedError, ValueError) as e:
        if 'Unknown mat file type' in str(e):
            pass
        elif isinstance(e, NotImplementedError):
            pass
        else:
            raise
    else:
        return fin
    try:
        finhdf5 = h5py.File(fin)
        fin = {}
        for key, array in finhdf5.iteritems():
            try:
                fin[key] = array.value
            except Exception as e:
                if "has no attribute 'value'" in str(e):
                    fin[key] = array
                else:
                    raise
    except Exception as e:
        raise
    return fin


def unique_rows(matrix):
    '''determines the unique rows of a matrix.

    Parameters
    ----------
    matrix : array_like, shape=[n, m]
        matrix.

    Notes
    -----
    If the given array is not contiguous, it
    '''
    matrix = np.ascontiguousarray(matrix)
    dt = matrix.dtype
    row_length = matrix.shape[1]
    return np.unique(matrix.view(dt.descr * row_length)
                     ).view(dt).reshape(-1, row_length)


def entropy(histogram, axis=-1):
    '''computes the simplest entropy estimate by computing the discrete entropy
    on the histogram.

    Parameters
    ----------
    histogram : array_like
        histogram for which the entropy shall be estimated.

    Returns
    -------
    H : float
        estimated entropy.
    '''
    histogram = histogram[histogram > 0]
    return -np.sum(histogram * np.log(histogram), axis=axis)


def loop_status(val, minval, maxval, text=None):
    '''prints a progress indicator that is overwritten each time it
    is called.


    Parameters
    ----------
    val : float
        current value.
    minval : float
        starting value.
    maxval : float
        final value.
    text : string
        text to be displayed next to progress.

    Notes
    -----
    This is not strictly tied to numpy, but it's very handy.
    '''

    if text is None:
        text = ''

    if minval - maxval == 0:
        return

    # lets see the progress...
    sys.stdout.write(
        "\r[%3d%%] %s" % (
            int(100 * (val - minval) / float(maxval - minval)), text))
    sys.stdout.flush()


def get_neighbours_fast(volume):
    '''computes edge list of adjacent supervoxels.

    Parameters
    ----------
    volume : array_like, shape=[image dimensions]
        image volume where each label represents the membership to a certain
        supervoxel.

    Returns
    -------
    adjacency : array_like, shape=[n_pairs, 2]
        adjacency representation in terms of edges.

    Notes
    -----
    All pairs are ordered such that the order of the ids of the pair is
    increasing. All pairs are unique.
    '''

    if len(volume.shape) == 3:
        pairs = np.ascontiguousarray(np.vstack(
            [np.vstack([volume[:-1, ...].flatten(), volume[1:, ...].flatten()]).T,
             np.vstack([volume[:, :-1, :].flatten(), volume[:, 1:,:].flatten()]).T,
             np.vstack(
                 [volume[..., :-1].flatten(), volume[..., 1:].flatten()]).T,
             ]))
    elif len(volume.shape) == 2:
        pairs = np.ascontiguousarray(np.vstack(
            [np.vstack([volume[:-1, ...].flatten(), volume[1:, ...].flatten()]).T,
             np.vstack([volume[:, :-1].flatten(), volume[:, 1:].flatten()]).T,
             ]))

    # clean up. (i.e. remove identical pairs)
    pairs = pairs[pairs[:, 0] != pairs[:, 1]]
    # swap if out of order.
    pairs[pairs[:, 0] > pairs[:, 1], :] = pairs[pairs[:, 0] > pairs[:, 1], ::-1]

    # now determine uniques.
    dt = pairs.dtype
    adjacency = np.unique(pairs.view(dt.descr * pairs.shape[1])
                          ).view(dt).reshape(-1, pairs.shape[1])
    return adjacency


def get_neighbours_fast_along_axis(volume, axis=0, unordered=False):
    '''computes edge list of adjacent supervoxels.

    Parameters
    ----------
    volume : array_like, shape=[image dimensions]
        image volume where each label represents the membership to a certain
        supervoxel.

    Returns
    -------
    adjacency : array_like, shape=[n_pairs, 2]
        adjacency representation in terms of edges.

    Notes
    -----
    All pairs are ordered such that the order of the ids of the pair is
    increasing. All pairs are unique.
    '''

    if axis > len(volume.shape):
        raise IndexError('Given axis does not exist!')

    if axis == 0:
        pairs = np.ascontiguousarray(np.vstack(
            [np.vstack([volume[:-1, ...].flatten(),
                        volume[1:, ...].flatten()]).T]))
    elif axis == 1:
        pairs = np.ascontiguousarray(np.vstack(
            [np.vstack([volume[:, :-1, ...].flatten(),
                        volume[:, 1:, ...].flatten()]).T]))
    elif axis == 2:
        pairs = np.ascontiguousarray(np.vstack(
            [np.vstack([volume[:, :, :-1, ...].flatten(),
                        volume[:, :, 1:, ...].flatten()]).T]))
    else:
        raise NotImplementedError()

    # clean up. (i.e. remove identical pairs)
    pairs = pairs[pairs[:, 0] != pairs[:, 1]]
    # swap if out of order.
    if not unordered:
        pairs[pairs[:, 0] > pairs[:, 1], :] = pairs[pairs[:, 0] > pairs[:, 1], ::-1]

    # now determine uniques.
    dt = pairs.dtype
    adjacency = np.unique(pairs.view(dt.descr * pairs.shape[1])
                          ).view(dt).reshape(-1, pairs.shape[1])
    return adjacency


def neighbourhood_stats(volume):
    '''computes the frequency of occurance of neighbourhood pairs.

    Parameters
    ----------
    volume : array_like, shape=[image dimensions]
        image volume.

    Returns
    -------
    histogram : array_like, shape=[xbins, ybins]
        neighbourhood histogram.
    '''
    pairs = np.ascontiguousarray(np.vstack(
        [np.vstack([volume[:-1, ...].flatten(), volume[1:, ...].flatten()]).T,
         np.vstack([volume[:, :-1, :].flatten(), volume[:, 1:,:].flatten()]).T,
         np.vstack([volume[..., :-1].flatten(), volume[..., 1:].flatten()]).T,
         ]))
    nbins = volume.max()
    histogr, _, _ = np.histogram2d(pairs[:, 0], pairs[:, 1], [nbins, nbins],
                                   normed=True)

    return histogr


def apply_argsort(a, sort_order=None, axis=-1):
    '''applies a sort order to a multidimensional array.

    TODO: doc
    '''
    idx = list(np.ogrid[[slice(x) for x in a.shape]])
    if sort_order is None:
        idx[axis] = a.argsort(axis)
    else:
        idx[axis] = sort_order
    return a[idx]


def isoresize(image, spacings):
    '''resize image such that all spacings are equivalent.

    Parameters
    ----------
    image : array_like, shape=[**]
        image to be resized.
    spacings : tuple or list
        spacing along each axis of the given image.
    '''
    assert len(image.shape) == len(spacings)
    min_spacing = float(np.min(spacings))
    target_zoom = [spacing / min_spacing for spacing in spacings]
    return zoom(image, zoom=target_zoom, order=3)


class Timer:

    '''
    timer class to be used as

    >>> with Timer('something') as t:
    >>>     do_something()
    >>> something took 1e-3 s
    '''

    def __init__(self, name='', verbose=True):
        self.name = name
        self.verbose = verbose

    def __enter__(self):
        # we use time.time here instead of time.clock() as multithreaded
        # operations would be measured wrong.
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.interval = self.end - self.start
        if self.verbose:
            print '{} took {:1.4e} s'.format(self.name, self.interval)


def chunks(array, chunksize=None):
    '''returns chunks of a given array along the first dimension.

    Parameters
    ----------
    array : array_like, shape=[*, **]
        array to be processed chunk-wise.

    Yields
    ------
    chunk : array_like, [chunksize, **]
        chunk of array.
    '''

    # some default values for chunksize.
    if chunksize is None:
        chunksize = min(1e5, len(array) / 10)

    ii = 0
    length = len(array)
    while ii < length:
        yield array[ii:min(ii + chunksize, length)]
        ii = ii + chunksize
