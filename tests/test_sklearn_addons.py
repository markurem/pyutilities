'''
unit test for ScoringDict


@author: Markus Rempfler
'''

from numpy_utils import ClassBalancer
import unittest

import numpy as np
from numpy.ma.testutils import assert_almost_equal, assert_array_equal
__updated__ = '14.12.2015'


class Test(unittest.TestCase):

    def setUp(self):
        np.random.seed(1337)

    def tearDown(self):
        pass

    def testClassBalancer(self):
        '''basic functionality of ClassBalancer.
        '''
        n_samples = 1000
        n_classes = 4
        p_target = np.asarray([1, 2, 3, 4]) / 10.
        probs = np.random.rand(n_samples, n_classes)
        target_labels = np.concatenate([[label, ] * int(100 * prob)
                                        for label, prob in enumerate(p_target)])
        balancer = ClassBalancer(n_classes=4)
        # check inference.
        balancer.fit(target_labels)
        assert_almost_equal(balancer.p_target, p_target)
        assert_almost_equal(balancer.p_source,
                            [1 / float(n_classes), ] * n_classes)

        # check if the output satisfies the requirements.
        adjusted = balancer.predict_proba(probs)
        assert_array_equal(adjusted.shape, probs.shape)
        assert_almost_equal(adjusted.sum(axis=-1), np.ones(adjusted.shape[0]))
        assert np.all(adjusted <= 1)
        assert np.all(0 <= adjusted)


if __name__ == "__main__":
    unittest.main()
