# README #

### What is this repository for? ###

* myplt : Utility functions for plotting in python.
* medvis : convenience function to work with medical image data
* numpy_utils : 
     * adds some utility functions that are missing in numpy/scipy
     * `Timer` to conveniently measure execution time. (This is not a replacement for `timeit`!)
     * `ClassBalancer` that adjusts probabilistic classifier outputs if the label distribution differs between training and application time.
     * `ScoringDict` for convenient benchmarking.


### Installation



Run either

```
python setup.py install
```

or

```
python setup.py develop
```

Tests can be launched automatically with:

```
python setup.py test
```

Note that in some cases, these commands need to be run with ```sudo```.


### CLI

The following command line scripts are provided:

```
matfileviewer.py  
npzviewer.py
opengm_solver_benchmark.py
```