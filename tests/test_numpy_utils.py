'''
unit test for numpy_utils.py

Created on Jan 13, 2015

@author: markus
'''

from numpy.testing import assert_array_equal
from numpy_utils import chunks
import numpy_utils
import unittest

import numpy as np
__created__ = '13.01.2015'
__updated__ = '30.06.2015'


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_unique_rows(self):
        '''test unique_rows.
        '''

        # Testcase A : array of pairs
        a = np.arange(10)
        b = np.arange(10, 20)

        c = np.vstack([a, b]).T

        expected = c

        rows = numpy_utils.unique_rows(c)

        self.assertTrue(
            np.all([row in expected.tolist() for row in rows.tolist()]))
        self.assertTrue(
            np.all([row in rows.tolist() for row in expected.tolist()]))

        # Testcase B : array of triplets (with redundancy)
        c = np.array([[1, 1, 2, 1, 2, 2, 2],
                      [1, 0, 1, 0, 1, 0, 1],
                      [1, 0, 1, 0, 1, 0, 0]]).T

        expected = np.array([[1, 0, 0],
                             [1, 1, 1],
                             [2, 0, 0],
                             [2, 1, 0],
                             [2, 1, 1]])

        rows = numpy_utils.unique_rows(c)

        self.assertTrue(
            np.all([row in expected.tolist() for row in rows.tolist()]))
        self.assertTrue(
            np.all([row in rows.tolist() for row in expected.tolist()]))

    def test_chunks(self):
        '''test iterator over chunks.
        '''
        testarray = np.arange(29)

        # check basic functionality.
        for ii, x in enumerate(chunks(testarray, 10)):
            assert_array_equal(x, np.arange(ii * 10, min((ii + 1) * 10, 29)))

        # test its usage in a generator.
        calc = np.concatenate([x ** 2 for x in chunks(testarray, 5)])
        assert_array_equal(testarray ** 2, calc)

        # test multidimensional case.
        testarray = np.arange(120).reshape(10, 12)
        calc = np.concatenate([x ** 2 for x in chunks(testarray, 7)])
        assert_array_equal(testarray ** 2, calc)

        testarray = np.arange(600).reshape(10, 12, 5)
        calc = np.concatenate([x ** 2 for x in chunks(testarray, 7)])
        assert_array_equal(testarray ** 2, calc)

    def test_isoresize(self):
        '''test resizing of images to isotropic spacing.
        '''
        image = np.random.randn(10, 15, 20)
        spacings = [0.5, 0.2, 0.3]
        resized = numpy_utils.isoresize(image, spacings)
        assert np.all((25, 15, 30) == resized.shape)

if __name__ == "__main__":
    unittest.main()
