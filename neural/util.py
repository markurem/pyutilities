'''utility to handle swc and related neuron tracing tasks.
'''

import numpy as np

from scipy.ndimage import distance_transform_edt


def rasterize_tracing(filename, dim):
    '''rasterizes tracings stored in an .swc file
    to volumentric annotations.

    Parameters
    ----------
    filename : string
        path to .swc file that contains annotation.

    Returns
    -------
    vol : array_like
        annotated volume.

    Notes
    -----
    Details about the .swc file format can be found here:
        http://www.neuronland.org/NLMorphologyConverter/MorphologyFormats/SWC/Spec.html
    '''
    z, y, x, rad = np.loadtxt(filename,
                              usecols=(2, 3, 4, 5)  # coordinates and radius.
                              ).T

    x = np.round(x).astype(int)
    y = np.round(y).astype(int)
    z = np.round(z).astype(int)
    
    vol = np.zeros(dim)

    # we round radii since we rasterize anyway.
    rad = np.round(rad, 1)
    for radius in np.unique(rad):
        mask = rad == radius


        dist = np.zeros(dim)
        dist[x[mask], y[mask], z[mask]] = 1
        distance_transform_edt(
            dist == 0, return_distances=True, distances=dist)

        vol = np.maximum(vol, dist <= radius)

    return vol
#     from myplt import volshow
#     volshow(vol.astype(np.float32))

if __name__ == '__main__':
    fin = '/home/markus/container/data/OPF/ground_truth/OP_1.swc'
    vol = rasterize_tracing(fin, dim=(60, 512, 512))
    from medvis import save
    
    from scipy.ndimage import maximum_filter, zoom
    vol = maximum_filter(vol, size=2)
    vol = np.round(zoom(vol, 0.5))
        
    
    save(image=vol.astype(np.uint8), path='/tmp/OP_1-gt.mha')
