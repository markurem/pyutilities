'''basic utility functions for handling medical image datasets.

@author: markus
'''
import datetime
import os
import tempfile
from warnings import warn

import SimpleITK as itk
try:
    import itk as ITK
except ImportError:
    pass
    #warn('ITK wrappers not found. Only limited functionality is provided.')
import numpy as np

try:
    import vtk
except ImportError:
    warn('vtk bindings not available.')

__created__ = '28.08.2014'
__updated__ = '21.01.2016'


def maximum_intensity_projection(image, axis=None,
                                 center=None, width=None):
    '''
    projects the maximum intensity value of a 3d-volume into
    a 2d image.

    Parameters
    ----------
    image : array_like
        image volume
    axis : int
        axis to project along (0,1,2)

    Returns
    -------
    projection : array_like
        projected image.
    '''

    # TODO: Sanity checks on image
    if axis is None:
        axis = -1
    elif axis >= len(image.shape):
        axis = -1

    if center is not None and width is not None:
        center = int(center)
        width = int(width)
        mask = np.arange(center - width, center + width + 1)
        mask = mask[np.logical_and(mask >= 0, mask < image.shape[axis])]

        if axis == 0:
            image = image[mask,:,:]
        elif axis == 1:
            image = image[:, mask,:]
        elif axis == 2:
            image = image[:,:, mask]

    return np.max(image, axis=axis)


def load(path, convert=False, roll=False):
    '''
    loads a medical image.

    Parameters
    ----------
    path : string
        path to image.
    convert : boolean
        if true, then an array is returned.
    roll : boolean
        if true, then the axis are rolled by -1.

    Returns
    -------
    image : itk image object.
    '''

    # check first whether the file exists (otherwise we get an ugly core dump).
    if not os.path.isfile(path):
        raise Exception('Given file does not exist! %s' % path)

    reader = itk.ImageFileReader()
    reader.SetFileName(path)
    image = reader.Execute()

    if convert and roll:
        arr = itk.GetArrayFromImage(image)
        return np.rollaxis(arr, 0, len(arr.shape))
    if convert:
        return itk.GetArrayFromImage(image)
    else:
        return image


def save(image, path, spacing=None):
    '''
    saves image to given location.

    Parameters
    ----------
    image : itk::image or array_like
        image to be saved.
    path : string
        path to saved image.

    Notes
    -----
    If the given path does not exist, it will be created.
    '''

    dirname = os.path.dirname(path)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

    writer = itk.ImageFileWriter()
    writer.SetFileName(path)
    image = convert_to_itk(image)
    if spacing is not None:
        image.SetSpacing(spacing)
    writer.Execute(image)


def read_dicom(path, verbose=False):
    '''
    reads dicom series.

    Parameters
    ----------
    path : string
        path to dicom series.
    verbose : boolean
        print out all series file names.

    Returns
    -------
    image : itk image
        image volume.
    '''
    reader = itk.ImageSeriesReader()
    names = reader.GetGDCMSeriesFileNames(path)
    if len(names) < 1:
        raise IOError('No Series can be found at the specified path!')
    elif verbose:
        print 'image series found in %s:\n\t' % path,
        print '\n\t'.join([os.path.basename(name) for name in names])
    reader.SetFileNames(names)
    image = reader.Execute()
    if verbose:
        print 'read image with size', [x for x in image.GetSize()]
    return image


def convert_dicom(base, path):
    '''
    converts dicom series to mhd volume.

    Parameters
    ----------
    base : string
        path to dicom series.
    path : string
        path to save new mhd volume.

    '''

    image = read_dicom(base, verbose=True)

    outdir = os.path.dirname(path)
    save(image, path)


def convert(image):
    '''
    converts from itk image to array and vice versa.

    Parameters
    ----------
    image : itk image or array_like
        image to be converted.

    Returns
    -------
    conv : itk image or array_like
        converted image.
    '''

    if isinstance(image, np.ndarray):
        return itk.GetImageFromArray(image)
    elif isinstance(image, itk.Image):
        return itk.GetArrayFromImage(image)
    else:
        raise TypeError('Given parameter is not a valid image.')


def convert_to_itk(image):
    '''
    always returns itk::image type of the given input image

    Parameters
    ----------
    image : itk image or array_like
        image to be converted.

    Returns
    -------
    conv : itk image
        image.
    '''

    if isinstance(image, np.ndarray):
        return itk.GetImageFromArray(image)
    elif isinstance(image, itk.Image):
        return image
    else:
        raise TypeError('Given parameter is not a valid image.')


def convert_to_array(image):
    '''
    always returns numpy array type of the given input image

    Parameters
    ----------
    image : itk image or array_like
        image to be converted.

    Returns
    -------
    conv : array_like
        image.
    '''

    if isinstance(image, np.ndarray):
        return image
    elif isinstance(image, itk.Image):
        return itk.GetArrayFromImage(image)
    else:
        raise TypeError('Given parameter is not a valid image.')


def masking(image, mask):
    '''
    masks image with given mask.

    Parameters
    ----------
    image : itk image
        image to be masked.
    mask : itk image
        mask to be applied.

    Returns
    -------
    masked : itk image
        masked image.
    '''

    return itk.Mask(image, mask)


def register(image_a, image_b, write_transform=None):
    '''
    registration.

    [WIP] do not use without care!

    Parameters
    ----------
    image_a : itk image
        static image
    image_b : itk image
        moving image
    write_transform : string
        path to write transformation to.

    Returns
    -------
    aligned_b : itk image
        aligned image_b


    Notes
    -----
    This method is strongly tuned to DSA data!
    '''

    warnings.warn('obsolete function! use register_simple instead!')

    ImageType = ITK.Image[ITK.UC, 3]

    readerA = ITK.ImageFileReader[ImageType].New(FileName=image_a)
    readerB = ITK.ImageFileReader[ImageType].New(FileName=image_b)

    readerA.Update()
    readerB.Update()

#     transform_type = 'affine'
    transform_type = 'trans'

    # TODO: handle this case dynamically.
#     metric = ITK.NormalizedCorrelationImageToImageMetric[ImageType, ImageType].New()
#     metric = ITK.MeanReciprocalSquareDifferenceImageToImageMetric[ImageType, ImageType].New()
    metric = ITK.MattesMutualInformationImageToImageMetric[
        ImageType, ImageType].New()
    optimizer = ITK.RegularStepGradientDescentOptimizer.New()
#     optimizer = ITK.ConjugateGradientOptimizer.New()
    interpolator = ITK.NearestNeighborInterpolateImageFunction[
        ImageType, ITK.D].New()
    if transform_type == 'affine':
        transform = ITK.AffineTransform[ITK.D, 3].New()
    elif transform_type == 'rigid':
        transform = ITK.Rigid3DTransform[ITK.D].New()
    elif transform_type == 'similarity':
        transform = ITK.Similarity3DTransform[ITK.D].New()
    elif transform_type == 'trans':
        transform = ITK.TranslationTransform[ITK.D, 3].New()
    elif transform_type == 'composite':
        transform = ITK.CompositeTransform[ITK.D, 3].New()
        transform.AddTransform(ITK.TranslationTransform[ITK.D, 3].New())
        transform.AddTransform(ITK.ScaleTransform[ITK.D, 3].New())


#     transform.SetIdentity()/
    # scales
    scales = ITK.Array[ITK.D](transform.GetNumberOfParameters())
    if transform_type in ['affine', 'rigid']:
        for ii in xrange(0, 9):
            scales[ii] = 10
        for ii in xrange(9, 12):
            scales[ii] = 0.1
        for ii in [2, 5, 6, 7]:
            scales[ii] = 100000
        scales[0] = 0.1
        scales[4] = 0.1
        scales[8] = 0.1
    elif transform_type in ['similarity', ]:
        for ii in xrange(3):
            scales[ii] = 1
        for ii in xrange(3, 6):
            scales[ii] = 0.0001
        scales[6] = 1
    elif transform_type == 'trans':
        for ii in xrange(transform.GetNumberOfParameters()):
            scales[ii] = 1
    try:
        optimizer.SetScales(scales)
    except RuntimeError:
        pass

    metric.SetInterpolator(interpolator)
    metric.SetTransform(transform)

    R = ITK.ImageRegistrationMethod[ImageType, ImageType].New()
    R.SetMetric(metric)      # ala bm.
    R.SetOptimizer(optimizer)
    R.SetTransform(transform)
    R.SetInterpolator(interpolator)     # ala bm.

    R.SetFixedImage(readerA.GetOutput())
    R.SetMovingImage(readerB.GetOutput())

    R.SetFixedImageRegion(readerA.GetOutput().GetLargestPossibleRegion())

    # handle the transformation properly for angio sequences.

    R.SetInitialTransformParameters(transform.GetParameters())
    optimizer.SetMaximumStepLength(1)
    optimizer.SetMinimumStepLength(0.001)
    optimizer.SetNumberOfIterations(200)
    optimizer.SetMinimize(True)

    try:
        print 'starting registration...'
        R.Update()
    except Exception as e:
        print 'FAILED!'
        print e
        return load(image_b)
    print 'success! Transformation:'
    tf = R.GetLastTransformParameters()
    if transform_type in ['affine', 'rigid']:
        A = np.zeros((3, 3))
        m = np.zeros((3,))
        for ii in xrange(9):
            A[ii / 3, ii % 3] = tf[ii]
        for ii in xrange(3):
            m[ii] = tf[ii + 9]
        print A, m
    elif transform_type in ['similarity', ]:
        A = np.zeros((3, 3))
        m = np.zeros((3,))
        for ii in xrange(3):
            A[ii, ii] = tf[ii]
        for ii in xrange(3):
            m[ii] = tf[ii + 3]
        print A, m, tf[6]
    elif transform_type == 'trans':
        for ii in xrange(3):
            print tf[ii]
    else:
        for ii in xrange(transform.GetNumberOfParameters()):
            print tf[ii]

    # resample moving image according to the computed transformation.
    resampler = ITK.ResampleImageFilter[ImageType, ImageType].New()
    resampler.SetInput(readerB.GetOutput())
    resampler.SetTransform(R.GetOutput().Get())
    resampler.SetOutputOrigin(readerA.GetOutput().GetOrigin())
    resampler.SetOutputSpacing(readerA.GetOutput().GetSpacing())
    resampler.SetOutputDirection(readerA.GetOutput().GetDirection())
    resampler.SetDefaultPixelValue(0)
    resampler.SetSize(readerA.GetOutput().GetLargestPossibleRegion().GetSize())

    resampler.Update()
    # converter = ITK.PyBuffer[ImageType]        # why does this not work?

    writer = ITK.ImageFileWriter[ImageType].New()
    tmp = os.path.join(tempfile.mkdtemp(), 'resampled.mhd')
    writer.SetFileName(tmp)
    writer.SetInput(resampler.GetOutput())
    writer.Write()

    return load(tmp)


def register_simple(fixed, moving,
                    optimizer=None, metric=None, transform=None,
                    logger=None):
    '''
    registration.

    [WIP] do not use without care!

    Parameters
    ----------
    fixed : itk image
        static image
    moving : itk image
        moving image

    Returns
    -------
    aligned_b : itk image
        aligned image_b
    '''

    fixed = convert_to_itk(fixed)
    moving = convert_to_itk(moving)

    # registration cannot handle uint images.
    fixed = itk.Cast(fixed, itk.sitkFloat32)
    moving = itk.Cast(moving, itk.sitkFloat32)

    # setup registration
    R = itk.ImageRegistrationMethod()

    # choice of metric
    if metric == 'Correlation':
        R.SetMetricAsCorrelation()
    elif metric == 'MutualInformation':
        R.SetMetricAsMattesMutualInformation(numberOfHistogramBins=50)
    elif metric == 'MeanSquares' or metric is None:
        R.SetMetricAsMeanSquares()

    # choice of optimizer
    if optimizer == 'RegularStepGradientDescent' or optimizer is None:
        R.SetOptimizerAsRegularStepGradientDescent(1.0, .01, 200)
    elif optimizer == 'LBFGSB':
        R.SetOptimizerAsLBFGSB()
    elif optimizer == 'GradientDescentLineSearch':
        R.SetOptimizerAsGradientDescentLineSearch(learningRate=1.0,
                                                  numberOfIterations=200,
                                                  convergenceMinimumValue=1e-5,
                                                  convergenceWindowSize=5)
    elif optimizer == 'ConjugateGradientLineSearch':
        R.SetOptimizerAsConjugateGradientLineSearch(learningRate=1.0,
                                                    numberOfIterations=200)

    # setup interpolation
    interpolator = itk.sitkLinear
    R.SetInterpolator(interpolator)

    # choose transformation
    if transform == 'Translation' or transform is None:
        transform = itk.Transform(fixed.GetDimension(), itk.sitkTranslation)
    elif transform == 'Similarity':
        transform = itk.Transform(fixed.GetDimension(), itk.sitkSimilarity)
    elif transform == 'Affine':
        transform = itk.Transform(fixed.GetDimension(), itk.sitkAffine)
    elif transform == 'Custom':
        warnings.warn('Custom transformation does not seem to work properly!')
        transform = itk.Transform(fixed.GetDimension(), itk.sitkComposite)
        transform.AddTransform(
            itk.Transform(fixed.GetDimension(), itk.sitkTranslation))
        transform.AddTransform(
            itk.Transform(fixed.GetDimension(), itk.sitkScale))

    R.SetInitialTransform(transform)

    if logger is not None:
        def command_iteration(method):
            logger.info("{0:3} = {1:10.5f} : {2}".format(method.GetOptimizerIteration(),
                                                         method.GetMetricValue(),
                                                         method.GetOptimizerPosition()))
        cout = logger.info
    else:
        def command_iteration(method):
            print("{0:3} = {1:10.5f} : {2}".format(method.GetOptimizerIteration(),
                                                   method.GetMetricValue(),
                                                   method.GetOptimizerPosition()))

        def cout(x):
            print x

    R.AddCommand(itk.sitkIterationEvent, lambda: command_iteration(R))

    cout('starting registration...')
    cout('')
    cout("  # =     metric : Position")
    outTx = R.Execute(fixed, moving)

    cout(80 * '-')
    cout(outTx)
    cout("Optimizer stop condition: {0}".format(
        R.GetOptimizerStopConditionDescription()))
    cout(" Iteration: {0}".format(R.GetOptimizerIteration()))
    cout(" Metric value: {0}".format(R.GetMetricValue()))

    resampler = itk.ResampleImageFilter()
    resampler.SetReferenceImage(fixed)
    resampler.SetInterpolator(interpolator)
    resampler.SetDefaultPixelValue(0)
    resampler.SetTransform(outTx)

    out = resampler.Execute(moving)
    return itk.Cast(out, itk.sitkUInt8)


def anisotropic_diffusion_filter(image, **kwargs):
    '''
    applies an anisotropic diffusion filter.

    Parameters
    ----------
    image : itk image
        image to be smoothed.

    Returns
    -------
    filtered : itk image
        filtered image.
    '''
    image = cast_to_float(image)
    return itk.GradientAnisotropicDiffusion(image, **kwargs)


def cast_to_float(image):
    '''cast ITK image voxel type to float.
    '''

    FLOAT = 8       # this is a float. (required by Corrector)

    if not image.GetPixelID() == FLOAT:
        return itk.Cast(image, FLOAT)
    else:
        return image


def n4_bias_field_correction(image, mask):
    '''
    applies nonparametric bias field correction.

    Parameters
    ----------
    image : itk image.
        image to be corrected.

    Returns
    -------
    corrected : itk image
        corrected image.
    '''
    image = cast_to_float(image)

    return itk.N4BiasFieldCorrection(image, mask)


def render_volume(data_matrix):
    '''renders a given volume in a new window using vtk.

    Parameters
    ----------
    data_matrix : array_like, shape[x, y, z]
        data array to be rendered.

    Notes
    -----
    This is based on the example from 
    http://www.vtk.org/Wiki/VTK/Examples/Python/vtkWithNumpy
    '''

    # get extent and ranges.
    minval = data_matrix.min()
    maxval = data_matrix.max()
    x_dim, y_dim, z_dim = data_matrix.shape
 
    # import data_matrix to vtk compatible form.
    dataImporter = vtk.vtkImageImport()
    data_string = data_matrix.tostring()
    dataImporter.CopyImportVoidPointer(data_string, len(data_string))
    dataImporter.SetDataScalarTypeToUnsignedChar()
    dataImporter.SetNumberOfScalarComponents(1)

    # set the visualisation range
    dataImporter.SetDataExtent(0, x_dim - 1, 0, y_dim - 1, 0, z_dim - 1)
    dataImporter.SetWholeExtent(0, x_dim - 1, 0, y_dim - 1, 0, z_dim - 1)

    # The following class is used to store transparencyv-values for later retrival. In our case, we want the value 0 to be
    # completly opaque whereas the three different cubes are given different transperancy-values to show how it works.
    alphaChannelFunc = vtk.vtkPiecewiseFunction()
    alphaChannelFunc.AddPoint(minval, 0.0)
    alphaChannelFunc.AddPoint(maxval, 0.9)

    # This class stores color data and can create color tables from a few color points. For this demo, we want the three cubes
    # to be of the colors red green and blue.
    colorFunc = vtk.vtkColorTransferFunction()
    colorFunc.AddRGBPoint(maxval, 0.0, 0.0, 0.0)
    colorFunc.AddRGBPoint(minval, 1.0, 1.0, 1.0)

    # The preavius two classes stored properties. Because we want to apply these properties to the volume we want to render,
    # we have to store them in a class that stores volume prpoperties.
    volumeProperty = vtk.vtkVolumeProperty()
    volumeProperty.SetColor(colorFunc)
    volumeProperty.SetScalarOpacity(alphaChannelFunc)

    # This class describes how the volume is rendered (through ray tracing).
    compositeFunction = vtk.vtkVolumeRayCastCompositeFunction()
    # We can finally create our volume. We also have to specify the data for it, as well as how the data will be rendered.
    volumeMapper = vtk.vtkVolumeRayCastMapper()
    volumeMapper.SetVolumeRayCastFunction(compositeFunction)
    volumeMapper.SetInputConnection(dataImporter.GetOutputPort())

    # The class vtkVolume is used to pair the preaviusly declared volume as well as the properties to be used when rendering that volume.
    volume = vtk.vtkVolume()
    volume.SetMapper(volumeMapper)
    volume.SetProperty(volumeProperty)

    # With almost everything else ready, its time to initialize the renderer and window, as well as creating a method for exiting the application
    renderer = vtk.vtkRenderer()
    renderWin = vtk.vtkRenderWindow()
    renderWin.AddRenderer(renderer)
    renderInteractor = vtk.vtkRenderWindowInteractor()
    renderInteractor.SetRenderWindow(renderWin)

    # We add the volume to the renderer ...
    renderer.AddVolume(volume)
    # ... set background color to white ...
    renderer.SetBackground(1, 1, 1)
    # ... and set window size.
    renderWin.SetSize(400, 400)

    # A simple function to be called when the user decides to quit the application.
    def exitCheck(obj, event):
        if obj.GetEventPending() != 0:
            obj.SetAbortRender(1)

    # Tell the application to use the function as an exit check.
    renderWin.AddObserver("AbortCheckEvent", exitCheck)

    renderInteractor.Initialize()
    # Because nothing will be rendered without any input, we order the first render manually before control is handed over to the main-loop.
    renderWin.Render()
    renderInteractor.Start()
