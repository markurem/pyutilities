from glob import glob
import os
from setuptools import setup, find_packages
# prepare setup.


def get_scripts(base):
    '''looks for .py files in scripts folder.
    '''
    # TODO: we should test whether the scripts run before installing them.
    scripts = glob(os.path.join(base, '*.py'))
    return scripts


# setup.
setup(name='pyutilites',
      version='0.1',            # version is kind of arbitrary.
      description='collection of utility functions for plotting, '
      'computing and data I/O.',
      author='Markus Rempfler',
      author_email='markurem@vision.ee.ethz.ch',
      license='MIT',
      packages=find_packages(exclude=['tests', 'expUtil',
                                      'exputil', 'netevalutils']),

      install_requires=['numpy',
                        'matplotlib>=1.4.3',
                        'scipy',
                        'h5py',
                        'scikit-learn',
                        'simpleitk',
                        'seaborn',
                        'gitpython'],

      # testing.
      test_suite='nose.collector',

      # CLI
      scripts=get_scripts('./shellscripts'),

      # I believe zipping makes the import rather slow.
      zip_safe=False
      )
