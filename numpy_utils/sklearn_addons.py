'''useful extensions for machine learning.
'''

from sklearn.base import BaseEstimator
from sklearn.base import ClassifierMixin
import numpy as np


class ClassBalancer(BaseEstimator, ClassifierMixin):

    '''adjusts for class imbalances during training. THe method
    used is a simple instance weighting.

    Parameters
    ----------
    p_source : array_like
        array describing the marginal source label distribution P_s(y).
    n_classes : int
        number of classes.

    Notes
    -----
    If no `p_source` is given, then a uniform distribution is
    assumed.

    References
    ----------
    J. Jiang "A literature survey on domain adaption of
        statistical classifiers.", (2008)
    N. Japkowicz and S. Stephen "The class imbalance problem:
        A systematic study.", (2002)

    TODO: doc
    '''

    def __init__(self,
                 p_source=None,
                 n_classes=2):
        self.n_classes = n_classes
        if p_source is not None:
            n_classes = len(p_source)
        if p_source is None:
            p_source = [1 / float(n_classes), ] * n_classes
        elif not len(p_source) == n_classes:
            raise AttributeError('n_classes and p_source do not match.')
        self.p_source = np.asarray(p_source)

    def fit(self, X, y=None):
        '''infers the marginal distribution of the labels from
        the given set X.

        Parameters
        ----------
        X : array_like
            Array of observed labels in target domain.
        '''
        # update n_classes if it isnt set properly.
        if not self.n_classes == X.max() + 1:
            self.n_classes = X.max() + 1
        # infer target marginal distribution P_t.
        total = float(len(X))
        self.p_target = [np.sum(X == label) / total
                         for label in xrange(self.n_classes)]
        self.p_target = np.asarray(self.p_target)

    def predict_proba(self, X):
        '''predicts with adjusted probability.

        Parameters
        ----------
        X : array_like, shape=[n_samples, n_classes]
            unadjusted probabilities, i.e. from source domain.

        Returns
        -------
        X_t : array_like, shape=[n_samples, n_classes]
            adjusted probabilities.

        P_t(y|x) = r(y) * P_s(y|x) / Z

        where r(y) = P_t(y) / P_s(y)
        and Z = \sum_{y \in Y} r(y) * P_s(y|x)
        '''
        r = self.p_target / self.p_source
        X *= r[None, :]
        X /= X.sum(axis=-1)[:, None]
        return X


if __name__ == '__main__':
    # TODO: check fails due to None default value in constructor.
    from sklearn.utils.estimator_checks import check_estimator
    check_estimator(ClassBalancer)
