from os.path import join, expanduser, basename
import os

from numpy import dot

import time
from githelper import get_branch_and_rev

import h5py


class Experiment(object):
    '''abstraction class for computational experiments stored
    in hdf5.
    '''

    def __init__(self, path):
        '''
        '''
        self.fin = h5py.File(path, 'a')
        self._n_cases = sum([1 for name in self.fin.keys()
                             if 'case' in name])

    def cases(self):
        '''returns an iterator over registered cases.
        '''
        return (Case(dset) for name, dset in self.fin.iteritems()
                if 'case' in name)

    def setup_case(self, data, description, short, expected=None):
        '''creates a new case for given input data.
        '''
        case_dset = self.fin.create_group('case-{:04}'.format(self._n_cases))
        case_dset.create_group('reports')
        case_dset.create_dataset('data',
                                 data=data,
                                 compression='gzip')
        if expected is not None:
            case_dset.create_dataset('expected',
                                     data=expected,
                                     compression='gzip')
        case_dset.attrs['description'] = description
        case_dset.attrs['short'] = short
        self._n_cases += 1

    def close(self):
        '''close file.
        '''
        self.fin.close()

    def __str__(self):
        s = 'Experiment:\n'
        s += '\tcases={}\n'.format(self._n_cases)
        for case in self.cases():
            s += str(case) + '\n'
        return s


class Case(object):

    def __init__(self, dset):
        '''
        '''
        self._dset = dset
        self.reports = dset['reports']   # group.

    @property
    def data(self):
        return self._dset['data'].value

    @property
    def expected(self):
        try:
            return self._dset['expected'].value
        except AttributeError:
            return None

    @property
    def short(self):
        try:
            return self._dset.attrs['short']
        except AttributeError:
            return None

    @property
    def description(self):
        try:
            return self._dset.attrs['description']
        except AttributeError:
            return None

    @property
    def n_reports(self):
        return len(self._dset['reports'].keys())

    def print_all_reports(self):
        def visitor(name, obj):
            if 'result' in basename(name):
                print obj.name
                print '\t',
                print '\n\t'.join(('{} : {}'.format(key, val)
                                   for key, val in obj.attrs.iteritems()))
                print '\tobjective value : {}'.format((self.data * obj['solution'].value).sum())

            return None
        self._dset.visititems(visitor)

    def reported(self):
        return (dset for name, dset in self._dset['reports'].iteritems()
                if 'result' in name)

    def get_reported(self, key=None, attribute=None):

        res = []
        for report in self.reported():
            if key in report.attrs['name']:
                try:
                    val = report.attrs[attribute]
                    res.append(val)
                except KeyError:
                    pass

        if len(res) == 0:
            return None
        else:
            return res
#         def visitor(name, obj):
#             if 'result' in basename(name):
#                 if key in obj['name']:
#                     try:
#                         val = obj.attrs[attribute]
#                         return

    def get_data(self):
        return self.data

    def __str__(self):
        s = 'case={}\n'.format(self._dset.name)
        s += '\tshort={}\n'.format(self.short)
        s += '\tdata={}\n'.format(self.data.shape)
        s += '\treports={}'.format(len(self._dset['reports'].keys()))
        return s

    def report(self,
               solution,
               name,
               **kwargs):
        '''reports results for a run on this testcase.

        TODO: doc
        '''
        n_reports = self.n_reports
        report = self._dset['reports'].create_group('result-{:04}'.format(
            n_reports))
        report.create_dataset('solution', data=solution,
                              compression='gzip')
        report.attrs['date'] = time.strftime("%d-%m-%Y")
        report.attrs['time'] = time.strftime("%H:%M:%S")
        report.attrs['name'] = name

        # get info on git rev.
        try:
            gitrev = get_branch_and_rev()
            report.attrs['git-branch'] = gitrev['branch']
            report.attrs['git-rev'] = gitrev['rev']
        except IOError:
            report.attrs['git-branch'] = 'unknown'
            report.attrs['git-rev'] = 'unknown'

        for key, val in kwargs.iteritems():
            report.attrs[key] = val
