#! /usr/bin/env python
'''
viewer for npz files.

Example
-------
$ ~/python/shellscripts/npzviewer.py some_file.npz

'''


import argparse
import h5py
import logging
from numpy_utils import loadmat
import os
import sys

import numpy as np
logging.basicConfig(level=logging.INFO,
                    format='%(levelname)-8s %(message)s',
                    )


def main():
    try:
        # parsing
        args = parse()
        # read file and show content.
        read_and_show_content(args)
    except Exception as e:
        logging.error('An error occured!\n%s' %
                      (type(e).__name__ + ': ' + str(e)))
        return 1
    return 0


def parse():
    '''parse command line arguments (and do some sanity checks?).
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='input file [.npz]')
    parser.add_argument('--full', help='show full data',
                        action='store_true')
    parser.add_argument('--scores',
                        help='assume the file contains scores and list them',
                        action='store_true')

    args = parser.parse_args()

    if args.input is None:
        logging.error('No input file given!')
        raise Exception()
    return args


def read_and_show_content(args):
    '''tries to load the mat file and show its content.
    '''

    if not os.path.isfile(args.input):
        logging.error('Given file does not exist!')
        raise Exception()

    if not os.path.splitext(args.input)[1] == '.npz':
        logging.error('Given file is not a .npz file!')
        raise Exception()

    try:
        fin = np.load(args.input)
        print ''
        print 'File: {}'.format(args.input)
        print ''

        print_content_information(fin)

        if args.scores:
            print_scores(fin)

        if args.full:
            print_full(fin)

    except Exception as e:
        raise


def print_scores(fin):
    '''assumes the file contains score-arrays and lists them.
    '''

    # this will raise an error if
    scores = fin['scores']
    try:
        indices = fin['indices']
    except KeyError as e:
        indices = range(scores.shape[0])

    print ''
    print '\t{:<20} : {:10}'.format('INDEX', 'SCORES [%]')
    print '\t' + (23 + (scores.shape[-1] * 9)) * '-'
    for idx, score in zip(indices, scores):
        fmt = '\t{:<20} : ' + len(score) * '{:>9.1f}'
        print fmt.format(idx, *score.astype(float) * 100)


def print_content_information(fin):
    '''prints information about all the actual data stored.
    '''

    print 'Data:'
    fmt = '\t{:<20} : {:20}'
    print fmt.format('IDENTIFIER', 'SPECS')

    for key, value in fin.iteritems():

        # skip default keys.
        if key in ['__version__', '__header__', '__globals__']:
            continue

        if isinstance(value, np.ndarray):
            #             print key, value.shape
            print fmt.format(key,
                             describe_ndarray(value))
        # untested.
        elif isinstance(value, str):
            print fmt.format(key, value)
        elif type(value) in (int, float, bool):
            print fmt.format(key, str(value))
        else:
            print fmt.format(key, type(value))


def print_full(fin):
    if len(fin.keys()) == 3:
        print 'given file does not contain any data!'
        return

    print ''
    print 'Data (full):'

    for key, value in fin.iteritems():
        # skip default keys.
        if key in ['__version__', '__header__', '__globals__']:
            continue

        print '\t{}'.format(key)
        print value


def describe_ndarray(value):
    '''generates a string that describes an ndarray.
    '''
    desc = 'ndarray ' + '(' + \
        'x'.join([str(x) for x in value.shape]) \
        + ', ' + str(value.dtype)
    try:
        desc += ', [' + '{:2.1f}'.format(value.min()) \
            + ', ' + '{:2.1f}'.format(value.max()) + ']' \
            + ')'
    except Exception as e:
        desc += ')'
    finally:
        return desc


if __name__ == '__main__':
    sys.exit(main())
