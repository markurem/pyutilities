'''Collection of custom plotting functions.

Notes
-----
This module needs matplotlib v1.4.3 for full functionality.
'''

import datetime
from matplotlib import cm
from matplotlib.patches import Polygon
from matplotlib.pyplot import figure
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
from scipy.stats import pearsonr
from sklearn import metrics
from sklearn.calibration import calibration_curve
from sklearn.preprocessing import label_binarize
import sys
from warnings import warn

import matplotlib.pyplot as plt
import numpy as np
# import seaborn to have a nice plotting style.
try:
    import seaborn
    seaborn.set_style('white')
    seaborn.set_style(
        "ticks", {
            "xtick.major.size": 4, "ytick.major.size": 4})
except:
    pass
# from pylab import quiver, quiverkey
try:
    import cv2
except ImportError as e:
    print('Failed to import Opencv! \n\tError: {}\n'
          'Limited functionality provided.')


def activate_texfont(fontsize=None, other=None):
    '''activates tex fonts for all plots in current session.

    Parameters
    ----------
    fontsize : int
        default font size for text in figures.
    '''
    params = {'text.usetex': True,
              'font.size': 7,
              'font.family': 'serif',
              'font.serif': 'Computer Modern Roman',
              'text.latex.unicode': True,
              'lines.linewidth': 0.1,
              #'box.linewidth': 0.1
              }

    if other is not None:
        params.update(other)

    if fontsize is not None:
        params['font.size'] = fontsize

    plt.rcParams.update(params)


def boxplot(x, hold=None, color=None, alpha=0.5, label=None,
            linewidth=1, markersize=1, positions=None, **kwargs):
    '''
    This boxplot function extends the regular matplotlib.pyplot boxplot
    by allowing to set a face color, a label and prints average values.

    Parameters
    ----------
    x : array_like, shape=[n_samples, n_categories]
        data to be visualised.
    '''

    if isinstance(x, np.recarray):
        y = np.empty((x.shape[0], len(x.dtype.names)))
        for ii, name in enumerate(x.dtype.names):
            y[:, ii] = x[name]
        x = y

    means = np.mean(x, axis=0)

    if positions is None:
        positions = np.arange(len(means)) + 1

    # make boxplot and hand over standard parameters.
    bp = plt.boxplot(x, hold=hold,
                     showmeans=True,
                     meanprops={'marker': '*',
                                'markerfacecolor': 'black'},
                     whiskerprops={'linestyle': '-'},
                     **kwargs)

    ax = plt.gca()

    if color is not None:
        # make box black, fliers and medians red.
        plt.setp(bp['boxes'], color='black', linewidth=linewidth)
        plt.setp(bp['whiskers'], color='black',
                 linewidth=linewidth, linestyle='-')
        plt.setp(bp['fliers'], color='red', linewidth=linewidth)
        plt.setp(bp['medians'], color='red', linewidth=linewidth)

        # color the boxes with the desired color.
        for box in bp['boxes']:
            boxX = []
            boxY = []
            for j in range(5):
                boxX.append(box.get_xdata()[j])
                boxY.append(box.get_ydata()[j])
            boxCoords = zip(boxX, boxY)
            boxPolygon = Polygon(
                boxCoords, facecolor=color, alpha=alpha, linewidth=linewidth)
            ax.add_patch(boxPolygon)

    # print black stars for average values.
#     plt.plot(positions, means, marker='*',
#              markersize=markersize, linestyle='', color='black')

    if label is not None:
        # make a dummy plot so as to have a label.
        plt.plot([0], color=color, alpha=alpha, label=label)

    return bp


def percentile_plot(ydata, percentile=None, xdata=None, color=None,
                    with_median=False, mediancolor='red', **kwargs):
    '''visualises mean and a certain percentile of a given sequence of
    samples.

    Parameters
    ----------
    data : array_like, shape=[n_points, n_samples]
        data to be visualised.
    percentile : [float, float]
        percentile to be shown.
    '''
    if xdata is None:
        xdata = np.arange(len(ydata))

    if percentile is None:
        percentile = [0., 1.0]
    means = np.mean(ydata, axis=1)
    yerr = np.abs(np.percentile(ydata, q=percentile, axis=1) - means)
#     yerr = np.ones((2, len(means)))
#     yerr[0] *= 0.5
    plt.errorbar(xdata, means, yerr=yerr,
                 color=color, **kwargs
                 )
    if with_median:
        plt.plot(xdata, np.median(ydata, axis=1),
                 marker='_', color=mediancolor,
                 linestyle='')


def correlation_plot(x, y, color=None, labels=None, **kwargs):
    '''correlation plot over two random variables.

    It will compute the Pearson r coefficient and its p-value as implemented
    in scipy.stats.pearsonr. Note that the p-value assumes bivariate gaussian
    distributions and needs a large sample size.

    Parameters
    ----------
    x : array_like
        samples from the first random variable.
    y : array_like
        samples from the second random variable.
    color : string, optional (default: None)
        color for marker.
    labels : array_like, optional (default: None)
        labels for each pair of (x,y).

    '''

    plt.plot(x, y,
             marker='o', mew=0, markersize=8,
             linestyle='',
             color=color, alpha=0.6)

    # p-value is nonsense as the sample size is too small, but the pearson
    # coefficient is nice.
    pearson_coeff, pval = pearsonr(x, y)

    print 'pearson:', pearson_coeff
    print 'p-value:', pval

    # linear regression
    A = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(A, np.array(y))[0]
    lower, upper = plt.xlim()
    x_space = np.linspace(lower, upper, 100)
    plt.plot(x_space, m * x_space + c, color='black', linestyle=':',
             label='r={:0.3f}\np={:0.3f}'.format(pearson_coeff, pval),
             ** kwargs)

    if labels is not None:
        for label, xi, yi in zip(labels, x, y):
            plt.annotate(
                label,
                xy=(xi, yi), xytext = (0, 5),
                textcoords = 'offset points', ha = 'center', va = 'bottom',
                bbox = dict(boxstyle='round,pad=0.5', fc='gray', alpha=0.1),
                #                 arrowprops = dict(
                #                     arrowstyle='->',
                #                     connectionstyle='arc3,rad=0'),
                fontsize=6)

    # appearance
    remove_border()
    plt.legend(frameon=False, fontsize=10)
    plt.grid(True)


def entropy_plot(H, **kwargs):
    '''nicely depicts an entropy map together with its colorbar.

    Parameters
    ----------
    H : array_like, shape=[n_pixels, m_pixels]
        entropy map.
    '''

    ax = kwargs.pop('ax', plt.gca())

    cmap = kwargs.pop('cmap', 'afmhot')
    vmin = kwargs.pop('vmin', 0)
    vmax = kwargs.pop('vmax', H.max())

    im = ax.matshow(H, cmap=cmap, vmin=vmin, vmax=vmax,
                    ** kwargs)

    # now add the heatmap
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im, cax=cax)


def plot_scores(scores, axarr=None, x=None, color='blue'):
    '''
    Method to plot each keyword of a recarray.

    '''
    # TODO: Add further keywords of common plt.plot()
    names = scores.dtype.names

    if axarr is None:
        if len(names) == 4:
            fig, axarr = plt.subplots(2, 2, sharey=True, sharex=True)
        else:
            fig, axarr = plt.subplots(1, len(names), sharey=True, sharex=True)
        axarr = axarr.reshape((-1,))

    for ii, name in enumerate(names):
        if x is not None:
            axarr[ii].plot(
                x, scores[name], linestyle='--', color=color, marker='o')
        else:
            axarr[ii].plot(
                scores[name], linestyle='--', color=color, marker='o')
        axarr[ii].set_xlabel(name)

    return axarr


def remove_border(axes=None, top=False, right=False, left=True, bottom=True):
    '''
    Minimize chartjunk by stripping out unnecesasry plot borders and axis ticks

    The top/right/left/bottom keywords toggle whether the corresponding plot
    border is drawn.
    '''

    ax = axes or plt.gca()
    ax.spines['top'].set_visible(top)
    ax.spines['right'].set_visible(right)
    ax.spines['left'].set_visible(left)
    ax.spines['bottom'].set_visible(bottom)

    # turn off all ticks
    ax.yaxis.set_ticks_position('none')
    ax.xaxis.set_ticks_position('none')

    # now re-enable visibles
    if top:
        ax.xaxis.tick_top()
    if bottom:
        ax.xaxis.tick_bottom()
    if left:
        ax.yaxis.tick_left()
    if right:
        ax.yaxis.tick_right()


def imshow_all(volume, **kwargs):
    '''
    creates plot where all slices of a volume are shown in one plot.

    Parameters
    ----------
    volume : array_like, shape = [image dimensions]
        image volume to be visualized.
    '''

    rsize = int(np.sqrt(volume.shape[0]))
    n_max = volume.max()
    n_min = volume.min()
    _, axarr = plt.subplots(rsize, rsize)
    plt.subplots_adjust(hspace=0, wspace=0)
    for frame, ax in zip(volume, axarr.ravel()):
        ax.matshow(frame, vmin=n_min, vmax=n_max, **kwargs)
        ax.axis('off')


def show():
    '''convenience function interface.
    '''

    plt.show()


def matshow(matrix, show_values=False, **kwargs):
    '''shows a matrix (as pyplot.matshow) but offers the possibility to
    annotate each field with its value.

    Parameters
    ----------
    matrix : array_like, shape=[n,m]
        matrix to be shown.
    show_values : bool, optional (default: False)
        if True, values will be annotated.
    '''

    ax = plt.gca()

    # additional arguments for text.
    fontsize = kwargs.pop('fontsize', 8)
    fontcolor = kwargs.pop('fontcolor', 'black')
    formatter = kwargs.pop('formatter', '{:1.3f}')

    ax.matshow(matrix, **kwargs)

    if show_values:
        for ii in xrange(matrix.shape[0]):
            for jj in xrange(matrix.shape[1]):
                ax.text(jj, ii, formatter.format(matrix[ii, jj]),
                        va='center', ha='center', fontsize=fontsize,
                        color=fontcolor)


def savefig(path, bbox_inches='tight', dpi=600, **kwargs):
    '''
    saves figure to the desired folder with the desired name.
    If the folder does not exist, it will be created.

    Parameters
    ----------
    path : string
        path to save figure.
    '''

    folder, name = os.path.split(path)
    if not os.path.isdir(folder):
        try:
            os.makedirs(folder)
        except OSError as e:
            warn('saving figure failed.')
            print e
            return

    print 'saving figure to %s' % path
    plt.savefig(path, bbox_inches=bbox_inches, dpi=dpi, **kwargs)


def computeF1Threshold(precision, recall, thresholds):
    '''Computes the F1 optimal threshold.

    This is a helper function for precision_recall_curve

    # TODO: doc
    '''

    # ref: scikit-learn.org // sklearn.metrics.f1_score
    f1 = 2 * (precision * recall) / (precision + recall)
    ind = np.argmax(f1)

    return ind, f1[ind]


def _multiclass_curves(func, labels, predictions, average=False, classes=None
                       ):
    '''core function for multiclass PR and ROC curves.
    '''
    uniques = np.unique(labels)
    if classes is not None:
        mask = np.array([x in classes for x in uniques])
        predictions = predictions[:, mask]
        uniques = classes
    labels = label_binarize(labels, classes=uniques)
    n_classes = labels.shape[-1]

    colors = cm.get_cmap('rainbow', n_classes)(np.linspace(0, 1, n_classes))

    for ii, label in enumerate(uniques):
        xx, yy, thresholds = func(
            labels[..., ii], predictions[..., ii])
        auc = metrics.auc(xx, yy)
        plt.plot(xx, yy,
                 color=colors[ii],
                 label='{} (AUC={:1.3f})'.format(label, auc))

    # microaverage
    if average:
        xx, yy, thresholds = func(labels.ravel(),
                                  predictions.ravel())
        auc = metrics.auc(xx, yy)
        plt.plot(xx, yy,
                 color='black',
                 label='{} (AUC={:1.3f})'.format('avg', auc))

    # plot styling.
    remove_border()
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.grid(True)


def multiclass_prc(labels, predictions, average=False,
                   **kwargs):
    '''plots Precision-Recall curves for every class by treating them as
    binary problems.

    Parameters
    ----------
    labels : array_like, shape=[n_samples,]
        true class labels.
    predictions : array_like, shape=[n_samples, n_classes]
        prediction probabilities or confidence values.
    average : boolean, optional (default: False)
        plot average curve by "microaverageing"
    '''

    # metrics.precision_recall_curve has a different ordering from
    # metrics.roc_curve, so we have to permute here...
    def prc(y, y_hat):
        precision, recall, thresholds = metrics.precision_recall_curve(
            y, y_hat)
        return recall, precision, thresholds

    _multiclass_curves(prc,
                       labels, predictions, average=average, **kwargs)

    plt.xlabel('Recall')
    plt.ylabel('Precision')


def precision_recall_threshold(labels, predictions, colors=None):
    '''generates a plot with recall-threshold and precision-threshold.

    Parameters
    ----------
    labels : array_like, shape=[n_samples,]
        true class labels.
    predictions : array_like, shape=[n_samples, n_classes]
        prediction probabilities or confidence values.
    '''

    style = {'Recall': ':', 'Precision': '-'}

    uniques = np.unique(labels)
    if len(uniques) > 2:
        mask = uniques > 0
        uniques = uniques[mask]
        predictions = predictions[:, mask]
        labels = label_binarize(labels, classes=uniques)
    else:
        uniques = [1, ]
        labels = labels[..., None]
        predictions = predictions[..., None]
    n_classes = len(uniques)

    if colors is None:
        colors = cm.get_cmap(
            'rainbow',
            n_classes)(
            np.linspace(
                0,
                1,
                n_classes))

    for ii, label in enumerate(uniques):
        precision, recall, thresholds = metrics.precision_recall_curve(
            labels[..., ii], predictions[..., ii])

        for name, score, in zip(['Precision', 'Recall'], [precision, recall]):
            plt.plot(np.concatenate([[0, ], thresholds]),
                     score,
                     color=colors[ii],
                     linestyle=style[name],
                     label='label={}'.format(label))

    remove_border()
    plt.grid(True)


def multiclass_roc(labels, predictions, average=False,
                   **kwargs):
    '''plots ROCs for every class by treating them as binary problems.

    Parameters
    ----------
    labels : array_like, shape=[n_samples,]
        true class labels.
    predictions : array_like, shape=[n_samples, n_classes]
        prediction probabilities or confidence values.

    '''

    _multiclass_curves(metrics.roc_curve,
                       labels, predictions,
                       average=average, **kwargs)

    # random process
    xx = np.linspace(1e-12, 1, 1000)
    plt.plot(xx, xx, ':', color=(0.6, 0.6, 0.6),)

    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')


def precision_recall_curve(labels, predictions,
                           auc=True, annotate=True,
                           color=None, label=None,
                           dice_label=None,
                           linestyle=None,
                           optmarker=None,
                           isolines=False,
                           min_iso=None,
                           alpha=0.8,
                           ** kwargs):
    '''
    computes and shows the precision recall curve.

    Parameters
    ----------
    labels : array_like, shape=[n_samples]
        array with ground truth labels.
    predictions : array_like, shape=[n_samples]
        array with prediction probabilities.

    Returns
    -------
    scores : dict
        dict containing precision, recall, f1 and pr-auc scores. The former
        three are computed at the optimal operating point.
    '''
    precision, recall, thresholds = metrics.precision_recall_curve(
        labels, predictions, **kwargs)

    if linestyle is None:
        linestyle = '-'

    if optmarker is None:
        optmarker = dict(marker='x',
                         color='red',
                         markersize=10,
                         alpha=0.8)

    ax = plt.gca()
    ax.plot(recall, precision,
            color=color if color is not None else 'green',
            alpha=alpha,
            label=label if label is not None else '',
            linestyle=linestyle)

    ind, f1max = computeF1Threshold(
        precision, recall, thresholds)
    ax.plot(recall[ind], precision[ind],
            label=r'Dice$_{\mathrm{%s}}$=%2.1f %%' % (dice_label, 100 * f1max)
            if dice_label else '',
            **optmarker)

    if annotate:
        xy_pos = np.array([recall[ind], precision[ind]])
        if np.all(xy_pos >= 0.8):
            xy_text_pos = 0.8 * xy_pos
        else:
            xy_text_pos = 1.2 * xy_pos
        ax.annotate(r'Dice$_{\mathrm{opt}}$ = %2.1f %%' % (100 * f1max),
                    xy=(recall[ind], precision[ind]), xycoords='data',
                    xytext=xy_text_pos, textcoords='data',
                    arrowprops=dict(facecolor='black',
                                    shrink=0.15,
                                    width=0.5,
                                    headwidth=2.5),
                    horizontalalignment='center', verticalalignment='center',
                    fontsize=10)

    plt.xlim([0, 1.0])
    plt.ylim([0, 1.0])
    plt.ylabel('Precision')
    plt.xlabel('Recall')

    ax.xaxis.set_ticks([0, 0.25, 0.5, 0.75, 1])
    ax.yaxis.set_ticks([0, 0.25, 0.5, 0.75, 1])
    ax.xaxis.set_minor_locator(MultipleLocator(0.05))
    ax.yaxis.set_minor_locator(MultipleLocator(0.05))

    prauc = metrics.auc(recall, precision)
    if auc:
        ax.text(s='AUC = %2.1f %%' % (prauc * 100),
                x=0.7, y=1.05, transform=ax.transAxes,
                horizontalalignment='center',
                verticalalignment='center',
                fontdict={'fontsize': 12})
#     ax.axhline(np.mean(prec), xmax = np.mean(rec), color = 'black', linestyle='dotted')
#     ax.axvline(np.mean(rec), ymax = np.mean(prec), color = 'black', linestyle='dotted')
#     ax.plot(np.mean(rec), np.mean(prec), color = 'black',
#             marker = 'o', markerfacecolor = 'none', markeredgewidth = 1)

    # draw F1-score isolines instead of linear grid.
    if isolines:
        if min_iso is None:
            min_iso = 0
        delta = 0.01
        x = np.linspace(min_iso, 1.0, 1 / delta)
        y = np.linspace(min_iso, 1.0, 1 / delta)
        X, Y = np.meshgrid(x, y)
        plt.contour(X, Y, 2 * X * Y / (X + Y), 8,
                    colors='grey',
                    alpha=1,
                    linewidths=0.75,
                    linestyles=':')

    else:
        plt.grid(axis='y', )
    remove_border()
    return dict(recall=recall[ind], precision=precision[ind],
                dice=f1max, prauc=prauc, threshold=thresholds[ind])


def raw_prc(precision, recall,
            auc=True, annotate=True,
            color=None, label=None,
            dice_label=None,
            linestyle=None,
            optmarker=None,
            isolines=False,
            min_iso=None,
            alpha=0.8,
            ** kwargs):
    '''use this method to produce precision recall curves from precomputed
    precision and recall arrays.
    '''
    if linestyle is None:
        linestyle = '-'

    if optmarker is None:
        optmarker = dict(marker='x',
                         color='red',
                         markersize=10,
                         alpha=0.8)

    ax = plt.gca()
    if len(recall.shape) == 2 and len(precision.shape) == 2:
        ax.errorbar(np.mean(recall, axis=-1), np.mean(precision, axis=-1),
                    xerr=np.std(recall, axis=-1),
                    yerr=np.std(precision, axis=-1),
                    alpha=alpha,
                    label=label,
                    color=color,
                    **kwargs)
        if alpha < 1:
            ax.plot(np.mean(recall, axis=-1), np.mean(precision, axis=-1),
                    color=color)

        try:
            prauc = metrics.auc(np.mean(recall, axis=-1),
                                np.mean(precision, axis=-1),
                                )
        except ValueError as e:
            idx = np.argsort(np.mean(recall, axis=-1))
            prauc = metrics.auc(np.mean(recall, axis=-1)[idx],
                                np.mean(precision, axis=-1)[idx],
                                )

    else:
        ax.plot(recall, precision,
                color=color if color is not None else 'green',
                alpha=alpha,
                label=label if label is not None else '',
                linestyle=linestyle)

        ind, f1max = computeF1Threshold(
            precision, recall, None)
        ax.plot(recall[ind], precision[ind],
                label=r'Dice$_{\mathrm{%s}}$=%2.1f %%' % (
                    dice_label, 100 * f1max)
                if dice_label else '',
                **optmarker)
        prauc = metrics.auc(recall, precision)

    if annotate:
        xy_pos = np.array([recall[ind], precision[ind]])
        if np.all(xy_pos >= 0.8):
            xy_text_pos = 0.8 * xy_pos
        else:
            xy_text_pos = 1.2 * xy_pos
        ax.annotate(r'Dice$_{\mathrm{opt}}$ = %2.1f %%' % (100 * f1max),
                    xy=(recall[ind], precision[ind]), xycoords='data',
                    xytext=xy_text_pos, textcoords='data',
                    arrowprops=dict(facecolor='black',
                                    shrink=0.15,
                                    width=0.5,
                                    headwidth=2.5),
                    horizontalalignment='center', verticalalignment='center',
                    fontsize=10)

    plt.xlim([0, 1.0])
    plt.ylim([0, 1.0])
    plt.ylabel('Precision')
    plt.xlabel('Recall')

    ax.xaxis.set_ticks([0, 0.25, 0.5, 0.75, 1])
    ax.yaxis.set_ticks([0, 0.25, 0.5, 0.75, 1])
    ax.xaxis.set_minor_locator(MultipleLocator(0.05))
    ax.yaxis.set_minor_locator(MultipleLocator(0.05))

    if auc:
        ax.text(s='AUC = %2.1f %%' % (prauc * 100),
                x=0.7, y=1.05, transform=ax.transAxes,
                horizontalalignment='center',
                verticalalignment='center',
                fontdict={'fontsize': 12})
#     ax.axhline(np.mean(prec), xmax = np.mean(rec), color = 'black', linestyle='dotted')
#     ax.axvline(np.mean(rec), ymax = np.mean(prec), color = 'black', linestyle='dotted')
#     ax.plot(np.mean(rec), np.mean(prec), color = 'black',
#             marker = 'o', markerfacecolor = 'none', markeredgewidth = 1)

    # draw F1-score isolines instead of linear grid.
    if isolines:
        if min_iso is None:
            min_iso = 0
        delta = 0.01
        x = np.linspace(min_iso, 1.0, 1 / delta)
        y = np.linspace(min_iso, 1.0, 1 / delta)
        X, Y = np.meshgrid(x, y)
        plt.contour(X, Y, 2 * X * Y / (X + Y), 8,
                    colors='grey',
                    alpha=1,
                    linewidths=0.75,
                    linestyles=':')

    else:
        plt.grid(axis='y', )
    remove_border()
    return prauc


def calibration_plot(labels, probabilities, n_bins=10,
                     **kwargs):

    pos_frac, mean_pred = calibration_curve(labels,
                                            probabilities,
                                            n_bins=n_bins)
    plt.plot(mean_pred, pos_frac, **kwargs)
    plt.plot([0, 1], [0, 1], linestyle=':', color='Grey')

    remove_border()
    plt.grid(True)
    plt.xlabel('Mean predicted probability')
    plt.ylabel('Fraction of positives')


def segmentation_difference(segm_a, segm_b, colors=None):
    '''returns a colormap showing where the two given binary segmentations
    differ from each other.

    Parameters
    ----------
    segm_a, segm_b : array_like, shape=[**]
        binary segmentations. Must have the same shape.

    Returns
    -------
    diff_map : array_like, shape=[**, 3]
        color map indicating differences in the two segmentations.
    '''
    assert np.all(segm_a.shape == segm_b.shape)

    # default colors
    if colors is None:
        colors = dict(fg=(0, 0, 0),
                      bg=(255, 255, 255),
                      a_not_b=(255, 255, 0),
                      b_not_a=(255, 0, 255),
                      )

#     if not segm_a.dtype == bool:
#         segm_a = segm_a.astype(bool, copy=False)
#     if not segm_b.dtype == bool:
#         segm_b = segm_b.astype(bool, copy=False)
    diff_map = np.zeros(segm_a.shape)
    diff_map = segm_a - segm_b + 2 * segm_a * segm_b

    # assign colors
#     diff_map[np.logical_and(segm_a, segm_b), ...] = colors['fg']
# diff_map[~segm_a * ~segm_b, ...] = colors['bg']
#     diff_map[segm_a * ~segm_b, ...] = colors['a_not_b']
#     diff_map[segm_b * ~segm_a, ...] = colors['b_not_a']

    return diff_map


def imshow(image, title=None):
    '''shows an image using opencv.

    Parameters
    ----------
    image : array_like, shape=[n_pixels, m_pixels]
        image to be visualised.
    '''
    if title is None:
        title = str(datetime.datetime.now())
    cv2.imshow(title, image)
    cv2.waitKey(0)


def volshow(image, title=None):
    '''visualises an image volume or sequence interactively.

    Parameters
    ----------
    image : array_like, shape=[n_pixels, m_pixels]
        image volume to be visualised.
    '''
    if title is None:
        title = 'Visualisation'
    cv2.namedWindow(title, cv2.WINDOW_NORMAL)
    ii = 0
    while 0 <= ii < len(image):
        frame = image[ii]
        cv2.imshow(title, frame)
        ii, _, _ = controls(ii, len(image), 0, 0, 1, 1)


def multi_volshow(images, title=None):
    '''visualises an image volume or sequence interactively.

    Parameters
    ----------
    image : list of array_like, shape=[n_pixels, m_pixels]
        image volumes to be visualised.
    '''
    n_frames = np.min([image.shape[0] for image in images])

    if title is None:
        title = 'Visualisation'
    cv2.namedWindow(title, cv2.WINDOW_NORMAL)
    ii, background = 0, 0
    while 0 <= ii < n_frames:
        frame = images[background][ii]
        cv2.imshow(title, frame)
        ii, background, _ = controls(
            ii, n_frames, background, 0, len(images), 1)


def multi_volshow_with_overlay(images, overlays, title=None):
    n_frames = np.min([image.shape[0] for image in images])
    n_overlays = len(overlays) + 1

    if title is None:
        title = 'Visualisation'
    cv2.namedWindow(title, cv2.WINDOW_NORMAL)
    ii, background, overlay = 0, 0, 0
    while 0 <= ii < n_frames:
        frame = images[background][ii]

        if overlay > 0:
            overlay_frame = overlays[overlay - 1]

        cv2.imshow(title, frame)
        ii, _, background = controls(
            ii, n_frames, 0, background, n_overlays, len(images))


def imwrite(path, image,
            mask=None, color=None, alpha=0.3, rescale=False):
    '''save an image to a path.

    TODO: doc
    '''
    dirname = os.path.dirname(path)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

    if rescale:
        image = image.astype(float)
        image -= image.min()
        image /= image.max()
        image *= 255
        image = image.astype(np.uint8)
        print 'rescaled image:', image.min(), image.max()

    if mask is not None:

        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

        if color is None:
            color = (0, 0, 255)

        labels = set(np.unique(mask)) - set([0, ])
        for label in labels:
            dummy = image.copy()
            dummy[mask == label] = color
            image = cv2.addWeighted(image, 1 - alpha, dummy, alpha, 0)

    cv2.imwrite(path, image)


def feature_importance_plot(clf, names=None,
                            sorted=False,
                            top=None
                            ):
    '''Visualises feature importances.

    Parameters
    ----------
    clf : classifier object
        classifier that has a feature_importances_ attribute.
    names : list of strings, optional
        names of all feature channels.
    sorted : bool, optional (default: False)
        orders features according to their importance
    top : int, optional
        shows only top n features in the plot.
    '''

    if isinstance(clf, np.ndarray):
        importances = clf
    else:
        importances = clf.feature_importances_

    if sorted:
        sorted = np.argsort(importances)
        if top is not None:
            sorted = sorted[-top:]
        if names is not None:
            names = np.array(names)[sorted]
        importances = importances[sorted]

    width = 0.5
    ind = np.arange(len(importances)) + 1
    plt.barh(ind - width / 2., importances, width,
             color='lightskyblue', linewidth=0)

    if names is not None:
        plt.yticks(ind, names, fontsize=8)
    else:
        plt.ylabel('Features')

    plt.xlabel('Importance')
    remove_border()
    plt.grid(axis='x')
    plt.ylim([0, ind[-1] + width])


def controls(ii, end, mode, background_mode,
             nbr_of_modes, nbr_of_backgrounds):
    '''utility function to handle interactive visualisation
    of sequences.

    Use [w,s] to toggle output mode
        [a,d] to return/proceed to previous/next frame
        [esc]   exit

    Parameters
    ----------
    ii : int
        frame counter.
    end : int
        length of the sequence.
    mode : int
        mode identifier.
    background_mode : int
        background mode identifier.
    nbr_of_modes : int
        number of different modes.
    nbr_of_backgrounds : int
        number of different background modes.

    Returns
    -------
    ii : int
        new frame counter.
    mode : int
        new display mode.
    background_mode : int
        new background mode.
    '''
    ch = cv2.waitKey(0)
    if ch == 1048603 or ch == 27:
        print 'closing everything...'
        ii = end + 999      # we assume this is run in a "while ii < end" loop
    elif ch & 0xff == ord('d'):
        ii += 1
        if ii >= end:
            ii -= 1
    elif ch & 0xff == ord('a'):
        ii -= 1
        if ii < 0:
            ii = 0
    elif ch & 0xff == ord('w'):
        mode = (mode + 1) % nbr_of_modes
    elif ch & 0xff == ord('s'):
        mode = (mode - 1) % nbr_of_modes
    elif ch & 0xff == ord(' '):
        background_mode = (background_mode + 1) % nbr_of_backgrounds

    if not ii >= end:
        sys.stdout.write('\r[{:>2}/{:>2}]'.format(ii, end - 1))
        sys.stdout.flush()
    return ii, mode, background_mode


def close_all():
    '''closes all open windows (opencv and pyplot)
    '''
    cv2.destroyAllWindows()
    plt.close('all')
